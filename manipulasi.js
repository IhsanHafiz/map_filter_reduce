let daftarMakanan = [
	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/2pc1side.png",
		deskripsi	: "2-pc Chickenjoy W/ 1 Side",
		harga		: "47.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/2pc2side.png",
		deskripsi	: "2-pc Chickenjoy W/ 2 Side",
		harga		: "50.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc2side.png",
		deskripsi	: "3-pc Chickenjoy W/ 2 Side",
		harga		: "57.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketa.png",
		deskripsi	: "6-pc Chickenjoy Bucket,  6-pc Burger Steak Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketb.png",
		deskripsi	: "6-pc Chickenjoy Bucket, 1 Spaghetti Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketc.png",
		deskripsi	: "6-pc Chickenjoy Bucket, 1 Palabok Family Pack, 3 Peach Mango Pies",
		harga		: "66.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2019/11/bucketd.png",
		deskripsi	: "10-pc Chickenjoy Bucket, 5 Steamed Rice,  5 Peach Mango Pies",
		harga		: "66.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	},	{
		fotoMakanan	: "https://jollibeeusa.com/wp-content/uploads/2018/11/3pc1side.png",
		deskripsi	: "3-pc Chickenjoy W/ 1 Side",
		harga		: "55.000"
	}
];

const body = document.querySelector(".food");

for(let data of daftarMakanan){
	body.innerHTML += `
			 <div class="row">
 						 <div class="col-sm-6">
 						 	<div class="card" style="width: 18rem; display: flex;">
  								<img class="card-img-top" src="${data.fotoMakanan}">
  								<div class="card-body">
    								<h5 class="card-title">${data.deskripsi}</h5>
    								<p class="card-text">Harga ${data.harga}</p>
    								<a href="#" class="btn btn-primary" style="background:#cd1338; border: none;">Pesan</a>
  								</div>
							</div>
						</div>
						
					</div>`;
					
}